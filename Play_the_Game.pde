/**
 * Play the Game.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * an animation of roulette you've never seen.
 * created 2019.08.31
 */

/**
 * DrawState : drawing state.
 */
interface DrawState {
  void draw();
};

/**
 * Track : manage track drawing.
 */
private class Track {
  
  float tHue;       // track hue
  float tRadius;    // track radius
  float sRadian;    // drawing start angle
  float pSize;      // drawing size
  float pDiv;       // gap of each drawing point
  int   tick;       // time tick
  int   sweepStart; // sweep start time
  int   sweepCnt;   // how long to sweep

  DrawState drawState, pattern, filler;
  
  Track(float _tHue, float _tRadius, float _sRadian, float _pSize, float _pDiv) {
    tHue    = _tHue;
    tRadius = _tRadius;
    sRadian = _sRadian;
    pSize   = _pSize;
    pDiv    = _pDiv;

    tick       = 0;
    sweepStart = 0;
    sweepCnt   = 30;

    pattern   = new Pattern();
    filler    = new Filler();
    drawState = filler;
  }


  public void circulate() {
    tick++;
    fill(tHue % 360.0, 30.0, 90.0, 100.0);
    drawState.draw();
    if (tick > sweepStart + sweepCnt) {
      drawState = pattern;
    }
  }
  
  public void sweep() {
    drawState  = filler;
    sweepStart = tick;
    tHue += 120.0;
    pDiv += 0.5; // it makes different drawing pattern.
  }

/**
 * Pattern : draw checker pattern.
 */
  private class Pattern implements DrawState {
    public void draw() {
      blendMode(DIFFERENCE);
      pushMatrix();
      // 0.0055 makes nice pattern. :)
      rotate(sRadian + tick * PI * (pDiv + 0.0055));
      ellipse(tRadius , 0.0, pSize * 0.5, pSize);
      popMatrix();
    }
  }

/**
 * Filler : fill with plain color.
 */
  private class Filler implements DrawState {
    public void draw() {
      blendMode(BLEND);
      pushMatrix();
      rotate(sRadian + tick * pSize / tRadius);
      rect(tRadius , 0.0, pSize, pSize);
      for (int i = 0; i < 2; i++) {
        rotate(PI * 2.0 / 3.0);
        rect(tRadius , 0.0, pSize, pSize);
      }
      popMatrix();
    }
  }
  
}

/**
 * main
 */
float baseHue = random(360.0);
ArrayList<Track> tracks = new ArrayList<Track>();

void setup() {
  size(720, 720);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  rectMode(CENTER);
  background(0.0, 0.0, 100.0, 100.0);
  smooth();
  noStroke();

  int   trackCnt = 6;
  float radius   = 80.0;
  float radian   = random(TWO_PI);
  float pDiv     = floor(random(1.0, 5.0)) * 0.1;
  
  for (int i = 0; i < trackCnt; i++) {
    float pSize = map(i, 0, trackCnt, 50.0, 40.0);
    tracks.add(new Track(
                       baseHue + 30.0 * (i % 3),
                       radius,
                       radian,
                       pSize,
                       pDiv
                       ));
    radius += pSize - 1.0; // (- 1.0) makes nice sweep.
    radian += PI * 2.0 / 3.0;
  }
}

void draw() {
  translate(width * 0.5, height * 0.5);

  if (frameCount % 300 == 0) {
    for (Track t : tracks) {
      t.sweep();
    }
  }
  
  for (Track t : tracks) {
    t.circulate();
  }
}


